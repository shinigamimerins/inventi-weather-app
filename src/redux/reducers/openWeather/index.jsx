const initialState = {
  queryRes: {},
  fetching: false,
};

export const weatherData = (state = initialState, action) => {
  switch (action.type) {
    case "@openWeather/FETCH_CITY_DATA_PENDING":
      return { ...state, fetching: true };
    case "@openWeather/FETCH_CITY_DATA_RESOLVED":
      return { ...state, queryRes: action.payload, fetching: false };
    case "@openWeather/FETCH_CITY_DATA_REJECTED":
      return { ...state, error: action.payload, fetching: false };

    default:
      return state;
  }
};
