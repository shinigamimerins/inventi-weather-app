export const fetchCityData = query => {
  let promiseFetch = new Promise((resolve, reject) => {
    fetch(
      `https://api.openweathermap.org/data/2.5/weather?q=${query}&APPID=fcb0d5b3f94374996926a683c83848c0&units=metric`
    )
      .then(response => {
        return response.json();
      })
      .then(data => {
        resolve(data);
      })
      .catch(error => reject(error));
  });

  return {
    type: "@openWeather/FETCH_CITY_DATA",
    payload: promiseFetch
  };
};