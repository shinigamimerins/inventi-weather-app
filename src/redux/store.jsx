import { createStore, applyMiddleware } from "redux";
import { combineReducers } from "redux";
import logger from "redux-logger";
import { weatherData } from "./reducers/openWeather";
import promiseMiddleware from "redux-promise-middleware";

const allReducers = combineReducers({
  weatherData
});

const store = createStore(
  allReducers,
  applyMiddleware(
    logger,
    promiseMiddleware({
      promiseTypeSuffixes: ["PENDING", "RESOLVED", "REJECTED"]
    })
  )
);

export default store;
