import React, { Component } from "react";
import WeatherBackground from '../components/WeatherBackground';
import TopBar from "../components/TopBar";
import Weather from "../components/Weather";

class App extends Component {
  render() {
    return (
      <WeatherBackground>
        <TopBar />
        <Weather />
      </WeatherBackground>
    );
  }
}

export default App;
