export const styles = {
  wrapper: {
    flexDirection: "row",
    height: 55,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "rgba(106, 107, 111, 0.23)"
  }
};
