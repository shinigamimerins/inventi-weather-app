import React from "react";
import SearchBar from "../SearchBar";
import AppBar from "@material-ui/core/AppBar";
import { withStyles } from "@material-ui/core";
import { styles } from "./style";
import logo from "../../assets/images/cloudy.svg";
import cls from "./style.module.scss";

class TopBar extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <AppBar className={classes.wrapper}>
        <div className={cls.left}>
          <h1>Weather </h1>
          <img src={logo} alt="logo" className={cls.logo} />
        </div>
        <SearchBar />
      </AppBar>
    );
  }
}

const TopBarWithStyles = withStyles(styles)(TopBar);

export default TopBarWithStyles;
