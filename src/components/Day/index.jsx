import React from 'react';
import WeatherIcon from 'react-icons-weather';
import windIcon from "../../assets/images/wind-sock.svg";
import humidityIcon from "../../assets/images/humidity.svg";
import cls from './style.module.scss';

export const Day = ({ temp, id, description, temp_min, temp_max, humidity, name, wind, hour }) => {
  return (
    <div>
      <h2 className={cls.city}>{name}</h2>
      <p>{hour}</p>
      <div className={cls.mainInfo}>
        <span>{Math.round(temp)}°C</span>
        <WeatherIcon name="owm" iconId={`${id}`} className={cls.icon} />
      </div>
      <p className={cls.description}>{description}</p>
      <div className={cls.moreInfo}>
        <div className={cls.details}>
          <p>min: {Math.round(temp_min)}°C</p>
          <p>max: {Math.round(temp_max)}°C</p>
        </div>
        <div className={cls.details}>
          <div className={cls.infoWrapper}>
            <img src={humidityIcon} alt="humidity icon" className={cls.icons} />
            <p>{Math.round(humidity)}%</p>
          </div>
          <div className={cls.infoWrapper}>
            <img className={cls.icons} src={windIcon} alt="wind sock" />
            <p>{wind}m/s</p>
          </div>
        </div>
      </div>
    </div>
  )
}