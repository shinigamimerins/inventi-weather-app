import React from "react";
import { Day } from "../Day";
import { connect } from "react-redux";
import cls from "./style.module.scss";

class Weather extends React.Component {

  render() {
    if (!this.props.weatherData.weather) {
      return <p className={cls.intro}>Please search for a city</p>;
    }
    const { temp, humidity, temp_min, temp_max } = this.props.weatherData.main;
    const { description, id } = this.props.weatherData.weather[0];
    return (
      <div className={cls.wrapper}>
      <Day
        className={cls.wrapper}
        temp={temp}
        humidity={humidity}
        temp_min={temp_min}
        temp_max={temp_max}
        description={description}
        id={id}
        wind={this.props.weatherData.wind.speed}
        name={this.props.weatherData.name}
      />
      </div>
    );
  }
}

export default connect(
  state => ({ weatherData: state.weatherData.queryRes }),
  null
)(Weather);
