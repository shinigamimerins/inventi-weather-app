import React from "react";
import { connect } from "react-redux";
import cls from "./style.module.scss";
import rainBg from "../../assets/images/rain.jpg";
import cloudsBg from "../../assets/images/clouds.jpg";
import clearBg from "../../assets/images/clear.jpg";
import mistBg from "../../assets/images/mist.jpg";
import snowBg from "../../assets/images/snow.jpg";
import thunderstormBg from "../../assets/images/thunderstorm.jpg";
import defaultBg from '../../assets/images/default.jpg';

const backgroundMap = {
  Drizzle: rainBg,
  Clouds: cloudsBg,
  Fog: mistBg,
  Mist: mistBg,
  Clear: clearBg,
  Snow: snowBg,
  Thunderstorm: thunderstormBg,
  Rain: rainBg,
  Default: defaultBg
};

class WeatherBackground extends React.Component {
  state = {
    weather: "Default"
  };

  componentDidUpdate() {
    if (!this.props.weatherData.weather) {
      return;
    }
    if (this.props.weatherData.weather[0].main !== this.state.weather) {
      this.setState({
        weather: this.props.weatherData.weather[0].main
      });
    }
  }

  componentDidMount() {
    this.preCacheImages([rainBg, cloudsBg, clearBg, mistBg, snowBg, thunderstormBg, defaultBg]);
  }

  preCacheImages = (imgArr) => {
    for (let i = 0; i < imgArr.length; i++) {
      let img = new Image();
      img.src = imgArr[i];
    }
  }

  render() {
    let backgroundUrl = backgroundMap[this.state.weather];
    // fallback in case of unexpected API response
    if (!backgroundUrl) {
      backgroundUrl = this.state.weather;
    }
    return (
      <div
        className={cls.wrapper}
        style={{ backgroundImage: `url(${backgroundUrl})` }}
      >
        {this.props.children}
      </div>
    );
  }
}

export default connect(state => ({ weatherData: state.weatherData.queryRes }))(
  WeatherBackground
);
