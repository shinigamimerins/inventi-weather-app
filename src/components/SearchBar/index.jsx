import React from "react";
import { connect } from "react-redux";
import { fetchCityData } from "../../redux/actions/openWeather";
import Search from "@material-ui/icons/Search";
import cls from "./style.module.scss";

class SearchBar extends React.Component {
  constructor() {
    super();
    this.state = {
      inputValue: ""
    };
  }

  handleChange = e => {
    this.setState({
      inputValue: e.target.value
    });
  };

  onEnter = e => {
    if (e.keyCode === 13) {
      this.props.fetchCityDataAction(this.state.inputValue);
    }
  };

  handleSubmit = () => {
    this.props.fetchCityDataAction(this.state.inputValue);
  };

  render() {
    return (
      <div className={cls.wrapper}>
        <input
          className={cls.searchInput}
          onChange={this.handleChange}
          onKeyDown={this.onEnter}
          value={this.props.inputValue}
          placeholder="Search"
        />
        <Search className={cls.searchIcon} onClick={this.handleSubmit} />
      </div>
    );
  }
}

export default connect(
  state => ({ weatherData: state.weatherData.queryRes }),
  {
    fetchCityDataAction: fetchCityData
  }
)(SearchBar);
