import React from "react";
import ReactDOM from "react-dom";
import App from "./App/App.jsx";
import { Provider } from "react-redux";
import store from "./redux/store";
import './main.scss';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
